class Bank_System

    # initializing accounts variable in hash with key as username and value as balance amount
    def initialize
        @accounts = Hash.new
        puts "********* WELCOME TO GURKHA BANK **********"
        get_user_accounts
    end

    # get options fromuser for login into account or create account
    def get_user_accounts
        puts "*******************"
        puts "1. Enter To your account."
        puts "2. Create account"
        puts "3. Exit"
        user_input = gets.chomp.to_i
        user_account_handler(user_input)
    end

    # Handle user account input either to login or create account
    def user_account_handler(user_input)
        case user_input
        when 1
            account_login
        when 2
            create_account
        when 3
            puts "Thank you!! See you soon.Good Bye!"
            exit 
        else
            puts "Invalid Input. Enter either 1,2 or 3."
        end
        get_user_accounts
    end

    # this is called after login,,thismethods asks for user to choose either to deposit,withdraw or go back
    def get_user_input(user_name)
        puts "******************************"
        puts "Hello, #{user_name} ! Welcome To Gurkha Bank"
        puts "Choose the following options"
        puts "1. Show your balance"
        puts "2. Deposit Amount"
        puts "3. Withdraw Amount"
        puts "4. Go Back"
        puts "5. About Me"
        puts "6. Exit"
        puts "*************************"
        choice = gets.chomp.to_i
        user_choice_handler(choice,user_name)
    end

    # Handle user input to deposit withdraw or show amount for individual logged in user
    def user_choice_handler(choice,user_name)
        case choice
        when 1
            show_balance(user_name)
        when 2
            cash_deposit(user_name)
        when 3
            cash_withdraw(user_name)
        when 4
            get_user_accounts
        when 5
            about_me(user_name)
        when 6 
            puts "Thank you!! See you soon.Good Bye!"
            exit 
        else
            puts "Invalid Input.Enter 1,2,3 or 4."
        end
        get_user_input(user_name)
    end

    # SHow balance of logged in user, from hash variable accounts where username is key and balance in value
    def show_balance(user_name)
        balance = @accounts[user_name].to_i
        puts "You current balance is Rs. #{balance}"
    end

    # Show details of logged in user
    def about_me(user_name)
        balance = @accounts[user_name]
        puts "Your account name is #{user_name}."
        puts "Your balance is #{balance}"
    end

    # user login method..checks if user is present or not,,
    def account_login
        puts "Enter your username ==>"
        user_name = gets.chomp
        if @accounts.has_key?(user_name)
            get_user_input(user_name)
        else
            puts "Account not found."
        end
    end

    # create account...at first it checks whether the username already exists?
    def create_account
        puts "Enter your username to create account ==>"
        user_name = gets.chomp
        unless @accounts.has_key?(user_name)
            @accounts.store(user_name,0)
            puts "Account created successfully."
        else
            puts "Account already exists."
        end
    end

    # deposits amount to hash value of username account
    def cash_deposit(user_name)
        puts "Enter the amount to deposit ==>"
        deposit_amount = gets.chomp.to_i
        if (500..500000).include?deposit_amount
            puts "Rs. #{deposit_amount} has been deposited successfully to your account."
            @accounts[user_name] += deposit_amount
        else
            puts "You can deposit amount only between Rs.500 and Rs.5,00,000."
        end
    end

    # deposit amount
    def cash_withdraw(user_name)
        puts "Enter the amount to withdraw ==>"
        withdraw_amount = gets.chomp.to_i    
        unless withdraw_amount < 500
            if withdraw_amount > @accounts[user_name]
                puts "Sorry!!You balance is less than Rs. #{withdraw_amount}"
            else
                puts "Rs. #{withdraw_amount} has been withdrawn successfully to your account."
                @accounts[user_name] -= withdraw_amount
            end
        else
            puts "Sorry!!You cant withdraw amount less than Rs. 500."
        end
    end
    
end

bank = Bank_System.new